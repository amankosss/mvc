<?php
class Contacts extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
        public function get_contacts($login = FALSE)
		{
	        if ($login === FALSE )
	        {
                $query = $this->db->get('contacts');
                return $query->result_array();
	        }
	        $query = $this->db->get_where('contacts', array('login' => $login));
	        return $query->row_array();
		}
		public function set_contact()
		{
		    $this->load->helper('url');

		    //$slug = url_title($this->input->post('login'), 'dash', TRUE);

		    $data = array(
		        'login' => $this->input->post('login'),
		        'name' => $this->input->post('name')
		    );

		    return $this->db->insert('contacts', $data);
		}
}