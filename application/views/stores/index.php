<div class="table" style="margin-left:10px;">
			<table class="my_table table table-striped table-bordered table-hover ">
				<thead>
					<tr>
						<?php 
							$table_headers = array("№","Название","Адрес","Компания");
							foreach ($table_headers as $key => $value) {
								echo "<td>" . $value . "</td>";
							}
						?>
					</tr>
				</thead>
				<tbody>
				<?php 
					$cnt= 1;
					foreach ($stores as $key => $store) {
						echo "<tr>";
							echo "<td>" . $cnt++ . 														"</td>";
							echo "<td><a href='stores/". $store["store_id"] ."'>" . 
									$store["name"] .	"</a></td>";
							echo "<td>" . $store["address"].												"</td>";
							echo "<td>" . $store["comp_id"].												"</td>";
						echo "</tr>";
					} 
				?>
				</tbody>
			</table>
		</div>