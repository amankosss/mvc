<ul class="b-text-list m-wide-text-2 g-article">
<br><br><br>
	<li style="border-bottom: 1px solid #eaf0f3;">
		<div class="gl-wrap">
			<div class="l-col-left" style="padding-right: 100px;">
				<h3>BHelp поможет вам:</h3>
				<ul>
					<li>Получить больше новых клиентов</li>
					<li>Увеличить число успешных сделок </li>
					<li>Повысить повторные продажи  </li>
					<li>Автоматизировать бизнес-процессы</li>
					<li>Упростить работу менеджеров </li>
					<li>Анализировать продажи</li>
					<li>Бесплатно и неограниченно!</li>
				</ul>
			</div>
			<div class="l-col-img m-bottom">
				<img height="390" style="margin: 0 40px 0 0;" src="<?php echo base_url('images/screen4.png');?>"/>
			</div>
		</div>
	</li>
	<br><br><br><br>
</ul>
<ul class="b-text-list m-wide-text-2 g-article">
	<li>
		<div class="gl-wrap m-cloud-right">
			<div class="l-col-right">
				<br>
				<br>
				<h2 class="m-cloud">Что такое BHelp</h2>
				<ul>
					<li>учет всех ваших лидов и контактов</li>
					<li>база ваших клиентов  </li>
					<li>автоматизация ежедневной работы менеджеров </li>
					<li>интеграция с 1С</li>
					<li>интеграция с виртуальной АТС</li>
					<li>интеграция с интернет-магазином</li>
					<li>сделки и коммерческие предложения</li>
					<li>каталог товаров и работа со счетами</li>
					<li>воронка продаж, отчеты </li>
					<li>мобильная CRM и многое другое. </li>
				</ul>
				
			</div>
			<div class="l-col-img">
				<a class="ge-modal-img m-image" href="/img/content/features/crm-voronka.png" style="margin: 0 42px 0 100px;"><img src="<?php echo base_url('images/screen5.png');?>" width=250 /></a>
			</div>
		</div>
	</li>
</ul>