<?php $main_url = "http://localhost/amankossskz/"; ?>
<html>
    <head>
            <title><?php echo $title; ?></title>
            <meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name='wmail-verification' content='5fda795469cf5b0eee47942c7bc10972' />
            <meta name="google-site-verification" content="qpNgtLZTz_ldrlGi2XwX_pp-jCyC4ZyVGrCCQKeCT08" />
            <meta name = "description" content = "Контроль бизнес операциями для малого бизнеса">
            <meta name="keywords" content="business,bhelp,bb,almaty,help">
			<meta property="og:image" content="<?php echo base_url('images/logo.png');?>"/>
			<link href="<?php echo base_url('styles/style.css');?>" rel="stylesheet">
			<link href="<?php echo base_url('styles/bootstrap.css');?>" rel="stylesheet" >
			 <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('images/logo.png');?>" />
    </head>
        <body>
	<header >
		<div class="container-fluid">
			<div class="row">
				<div class="navbar navbar-inverse" style="background-color: #000;">
					<div class="container-fluid"  styles="border-radius:0px;">
						<div class="navbar-header">
							<a href="<?php echo base_url('index.php/home');?>" class="navbar-brand" >Главная</a>
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu">
								<span class="sr-only" >Open the navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="responsive-menu">
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<span class="glyphicon glyphicon-tasks"></span> Операции <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-plus-sign"></span> Оприходовать товар</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-ok-sign"></span> Продать товар</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-remove-sign"></span> Возврат товара</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-share-alt"></span> Перемещение товара</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-check"></span> Новый товар</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-plus-sign"></span> Приток на кассу</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-minus-sign"></span> Отток с кассы </a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-check"></span> Новая операция с кассой</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-check"></span> Новая категория товаров</a></li>
									</ul>
								</li>
								<li >
									<a href="<?php echo base_url('index.php/home');?>"  role="button" aria-haspopup="true" aria-expanded="false">
										<span class="glyphicon glyphicon-th-list"></span> История </a>
								</li>
							<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<span class="glyphicon glyphicon-list-alt"></span> Списки <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo base_url('index.php/home');?>"> Продажи</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Возвраты</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Перемещения</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Оприходования</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Оттоки-Притоки</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Магазины</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Типы оттоков притоков</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Категории товаров</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="<?php echo base_url('index.php/home');?>">Сотрудники</a></li>
									</ul>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li><input class="form-control" type="search" placeholder="Вы ищете..." style="margin-top: 8px;"></li>
								<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-search"></span> </a></li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
												<span class="glyphicon glyphicon-education"></span> Языки <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="<?php echo base_url('index.php/home');?>"> Русский</a></li>
												<li><a href="<?php echo base_url('index.php/home');?>">English</a></li>
												<li><a href="<?php echo base_url('index.php/home');?>">Қазақ</a></li>
											</ul>
										</li>
										<li><a href="<?php echo base_url('index.php/about');?>"><span class="glyphicon glyphicon-info-sign"></span> О сайте</a></li>
										<li><a href="<?php echo base_url('index.php/home');?>"><span class="glyphicon glyphicon-log-out"></span> Выход</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div style="height:120px; width:100%;">
		.
	</div>
        