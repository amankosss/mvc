<?php
class Bhelp extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('contacts');
                $this->load->helper('url_helper');
                $this->load->library('session');
        }

        public function index()
        {
                $data['contacts'] = $this->contacts->get_contacts();
                $data['title'] = "Друзья";//$data['news_item']['title'];
                $this->load->view('templates/header', $data);
                $this->load->view('contacts/index', $data);
                $this->load->view('templates/footer');
        }

        public function eng()
        {
                if($login===NULL) index();
                $data['contact'] = $this->contacts->get_contacts($login);
                $data['title'] = "Друг";//$data['news_item']['title'];
                $this->load->view('templates/header', $data);
                $this->load->view('contacts/view', $data);
                $this->load->view('templates/footer');
        }
        public function kaz()
        {
                if($login===NULL) index();
                $data['contact'] = $this->contacts->get_contacts($login);
                $data['title'] = "Друг";//$data['news_item']['title'];
                $this->load->view('templates/header', $data);
                $this->load->view('contacts/view', $data);
                $this->load->view('templates/footer');
        }

}
?>