<?php
class Stores extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('store');
                $this->load->helper('url_helper');
       }
        public function index()
        {
                $data['stores'] = $this->store->get_stores(FALSE);
                if (empty($data['stores']))
                {
                        exit("404");
                        show_404();
                }
                //exit("checkpoint NEWS array()");
                $data['title'] = "Магазины";//$data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('stores/index', $data);
                $this->load->view('templates/footer');
        }

        public function view($store_id = NULL)
        {
                //if($slug===NULL) index();
                $data['store'] = $this->store->get_stores($store_id);

                if (empty($data['store']))
                {
                        show_404();
                }

                $data['title'] = $data["store"]["name"];
                $this->load->view('templates/header', $data);
                $this->load->view('stores/view', $data);
                $this->load->view('templates/footer');
        }
}
?>