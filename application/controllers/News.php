<?php
class News extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('news_model');
                $this->load->helper('url_helper');
                 if(isset($_SESSION['site_lang']))
                    $this->lang->load('bhelp', $_SESSION['site_lang']);
                else
                    $this->lang->load('bhelp');
        }

        public function switchLanguage($language="")
        {
            $language = ($language != "") ? $language : "english";
            
            $this->session->set_userdata('site_lang', $language);
    
            redirect($_SERVER['HTTP_REFERER']);

        }
        public function index()
        {
                $data['news'] = $this->news_model->get_news();
                if (empty($data['news']))
                {
                        show_404();
                }
                //exit("checkpoint NEWS array()");
                $data['title'] = lang("title");//$data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('news/index', $data);
                $this->load->view('templates/footer');
        }

        public function view($slug = NULL)
        {
                //if($slug===NULL) index();
                $data['news_item'] = $this->news_model->get_news($slug);

                if (empty($data['news_item']))
                {
                        show_404();
                }

                $data['title'] = "Новости";//$data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('news/view', $data);
                $this->load->view('templates/footer');
        }
        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a news item';

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'Text', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('news/create');
                $this->load->view('templates/footer');

            }
            else
            {
                $this->news_model->set_news();
                $this->load->view('news/success');
            }
        }
}
?>