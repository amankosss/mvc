<?php
class Friends extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('contacts');
                $this->load->helper('url_helper');
                $this->load->library('session');
        }

        public function index()
        {
                $data['contacts'] = $this->contacts->get_contacts();
                if (empty($data['contacts']))
                {
                        show_404();
                }
                //exit("checkpoint NEWS array()");
                $data['title'] = "Друзья";//$data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('contacts/index', $data);
                $this->load->view('templates/footer');
        }
        public function change($word){

            $new_text = $word;

            $src = "/Users/mabookpro/Desktop/test.php";
            $myfile = fopen($src, "r") or die("Unable to open file!");
            $initial_value = fread($myfile,filesize($src));
            fclose($myfile);

            $md5_old = md5($initial_value);
            echo "MD5 of file : " . $md5_old . "<br>";
            
            $myfile = fopen($src, "w") or die("Unable to open file!");
            fwrite($myfile,$new_text);
            fclose($myfile);
            echo "We changed text to : " . $new_text . "<br>";
            
            $myfile = fopen($src, "r") or die("Unable to open file!");
            $new_value = fread($myfile,filesize($src));
            fclose($myfile);

            $md5_new = md5($new_value);
            echo "MD5 of file : " . $md5_new . "<br>";
            if($md5_new==$md5_old)
            echo "Same";
            else echo "Not the same";
        }
        public function view($login = NULL)
        {
                if($login===NULL) index();
                $data['contact'] = $this->contacts->get_contacts($login);
                if (empty($data['contact']))
                {
                        show_404();
                }
                $data['title'] = "Друг";//$data['news_item']['title'];
                $this->load->view('templates/header', $data);
                $this->load->view('contacts/view', $data);
                $this->load->view('templates/footer');
        }
        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a friend item';

            $this->form_validation->set_rules('login', 'Login', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('contacts/create');
                $this->load->view('templates/footer');

            }
            else
            {
                $this->contacts->set_contact();
                $this->load->view('contacts/success');
            }
        }
}
?>